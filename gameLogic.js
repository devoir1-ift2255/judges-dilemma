
class GameStateManager {
    constructor(){

    }
}


//An act is a collection of court case the player must survive through.
//An act is won when the player finishes the last case of this act
//and still has at least one reputation point.
class Act {
    constructor(actLength, cases){
        this.actLength = actLength; //the number of cases in this act.
        this.cases = cases; //An array of case objects describing each court case.
        this.status = status; //Whenever the act is won or not (boolean)
    }   
}

class Case {
    constructor(situation, defenseCase, prosecutorCase){
        this.situation =  situation; //the context of the court case

        // arrays containing text and images links;
        this.defenseCase = defenseCase; // the evidence to support innocence
        this.prosecutorCase = prosecutorCase; // the evidence for guilt
    }
}

//the player role plays a Judge which is tasked to make difficult 
//decision and to be affected by their consequences.
class Player {
    constructor(reputationPts,nbActWon,currentCase,currentAct,currentJudgment,currentPunishment){
        this.reputationPts =  reputationPts;
        this.nbActWon = nbActWon;
        this.currentCase = currentCase;
        this.currentAct = currentAct;
        this.currentJudgment = currentJudgment; //Judgement given by player
        this.currentPunishment = currentPunishment; //Punishment given by player
    }
}

//this will keep track of the player judgments & punishment
//and decide the appropriate consequences for the player.
//This will mainly decide how the player's reputation points are affected
//as well as what the next case will be about.
class ConsequencesManager {

}